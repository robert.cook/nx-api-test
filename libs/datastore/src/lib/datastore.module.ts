import { Module } from '@nestjs/common';
import { UserDatastoreV1 } from './user.v1.datastore';

@Module({
  providers: [UserDatastoreV1],
  exports: [UserDatastoreV1],
})
export class DatastoreModule {}
