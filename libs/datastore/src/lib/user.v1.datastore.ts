import { Injectable } from '@nestjs/common';
import { User } from '@nx-api-test/models';

@Injectable()
export class UserDatastoreV1 {
  async getUsers(): Promise<User[]> {
    const user = new User({ email: 'test@example.com' });
    return [user];
  }
}
