import { User } from './user';

describe('User', () => {
  it('should initialize id', () => {
    const user = new User({ email: 'someone@test.example.com' });
    expect(user.id).toBeDefined();
  });
});
