import { randomUUID } from 'crypto';

export interface UserFields {
  id?: string;
  email: string;
}

export class User {
  id: string;
  email: string;

  constructor(data?: UserFields) {
    this.id = data?.id || randomUUID();
    this.email = data?.email || '';
  }
}
