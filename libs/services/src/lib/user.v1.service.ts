import { Injectable } from '@nestjs/common';
import { User } from '@nx-api-test/models';
import { UserDatastoreV1 } from '@nx-api-test/datastore';

@Injectable()
export class UserServiceV1 {
  constructor(private readonly userDatastore: UserDatastoreV1) {}

  async getUsers(): Promise<User[]> {
    const users = await this.userDatastore.getUsers();
    return users;
  }
}
