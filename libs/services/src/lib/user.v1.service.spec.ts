import { Test, TestingModule } from '@nestjs/testing';
import { DatastoreModule } from '@nx-api-test/datastore';
import { UserServiceV1 } from './user.v1.service';

describe('UserServiceV1', () => {
  let service: UserServiceV1;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [DatastoreModule],
      providers: [UserServiceV1],
    }).compile();

    service = module.get<UserServiceV1>(UserServiceV1);
  });

  it('getUsers', async () => {
    const users = await service.getUsers();
    expect(users).toBeDefined();
  });
});
