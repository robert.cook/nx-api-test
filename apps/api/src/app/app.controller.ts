import { Controller, Get } from '@nestjs/common';
import { User } from '@nx-api-test/models';
import { UserServiceV1 } from '@nx-api-test/services';

import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly userService: UserServiceV1
  ) {}

  @Get()
  getData() {
    return this.appService.getData();
  }

  @Get('users')
  async getUsers(): Promise<User[]> {
    const users = await this.userService.getUsers();
    return users;
  }
}
