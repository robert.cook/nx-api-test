import { Module } from '@nestjs/common';
import { ServicesModule } from '@nx-api-test/services';

import { AppController } from './app.controller';
import { AppService } from './app.service';

@Module({
  imports: [ServicesModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
