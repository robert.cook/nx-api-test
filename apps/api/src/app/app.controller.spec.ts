import { Test, TestingModule } from '@nestjs/testing';
import { ServicesModule } from '@nx-api-test/services';

import { AppController } from './app.controller';
import { AppService } from './app.service';

describe('AppController', () => {
  let app: TestingModule;

  beforeAll(async () => {
    app = await Test.createTestingModule({
      imports: [ServicesModule],
      controllers: [AppController],
      providers: [AppService],
    }).compile();
  });

  describe('getData', () => {
    it('should return "Welcome to api!"', () => {
      const appController = app.get<AppController>(AppController);
      expect(appController.getData()).toEqual({ message: 'Welcome to api!' });
    });
  });

  describe('getUsers', () => {
    it('should return users', async () => {
      const appController = app.get<AppController>(AppController);
      const users = await appController.getUsers();
      expect(users).toBeDefined();
      expect(Array.isArray(users)).toBeTruthy();
    });
  });
});
